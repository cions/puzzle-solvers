#!/usr/bin/python

import argparse
import itertools
import re
import sys
import textwrap
from contextlib import nullcontext, redirect_stdout

import numpy as np
from ortools.sat.python import cp_model


re_pzprurl = re.compile(
    r'(?:https?://pzv\.jp/p\.html\?)?nurikabe/(\d+)/(\d+)/(.+)', re.ASCII)


def load_pzprurl(cols, rows, data):
    cols = int(cols)
    rows = int(rows)

    problem = np.zeros((rows, cols), dtype='uint64').reshape(-1)

    i = 0
    number = 0
    left = 1
    offset = 0
    for x in data:
        if x in '0123456789abcdef':
            number = (number << 4) | int(x, 16)
            left -= 1
            if left <= 0:
                problem[i] = number + offset
                i += 1
                number = 0
                left = 1
                offset = 0
        elif x == '-':
            left = 2
            offset = 0
        elif x == '+':
            left = 3
            offset = 0
        elif x == '=':
            left = 3
            offset = 4096
        elif x == '%':
            left = 3
            offset = 8192
        elif x == '*':
            left = 4
            offset = 12240
        elif x == '$':
            left = 5
            offset = 77776
        elif x.islower():
            i += int(x, 36) - 15

    return problem.reshape((rows, cols))


def load_pzprv3file(f):
    if f.readline() != 'pzprv3\n':
        raise ValueError('invalid format')
    if f.readline() != 'nurikabe\n':
        raise ValueError('invalid format')

    rows = int(f.readline())
    cols = int(f.readline())

    problem = np.zeros((rows, cols), dtype='uint64')

    for i in range(rows):
        row = f.readline().split()
        for j, x in enumerate(row):
            if x in ('.', '#', '+'):
                continue
            problem[i, j] = int(x)

    return problem


class Pzprv3FileOutputter:
    def __init__(self, dest):
        self.dest = dest

    def __call__(self, problem, solution):
        if self.dest != '-':
            cm = open(self.dest, 'w')
        else:
            cm = nullcontext(sys.stdout)
        with cm as f, redirect_stdout(f):
            print('pzprv3')
            print('nurikabe')
            print(problem.shape[0])
            print(problem.shape[1])

            sol = np.where(solution, '#', '+')
            for row in np.where(problem != 0, problem, sol):
                print(' '.join(str(x) for x in row))


def SolutionPrinter(problem, solution):
    w = len(str(problem.max()))
    sol = np.where(solution, '#' * w, '.')
    for row in np.where(problem != 0, problem, sol):
        print(' '.join(str(x).center(w) for x in row))
    print()


class SolutionCallback(cp_model.CpSolverSolutionCallback):
    def __init__(self, problem, variables, outputter):
        super().__init__()
        self.problem = problem
        self.variables = variables
        self.outputter = outputter
        self.nsolutions = 0

    def on_solution_callback(self):
        self.nsolutions += 1

        solution = np.zeros_like(self.problem, dtype='bool')
        for k, v in self.variables.items():
            solution[k] = self.BooleanValue(v)
        self.outputter(self.problem, solution)

    def number_of_solutions(self):
        return self.nsolutions


def solve(problem, outputter, find_all=False, timeout=None):
    rows, cols = problem.shape

    nnumbers = np.count_nonzero(problem != 0)
    numberids = {}
    for i, j, k in zip(*np.where(problem != 0), itertools.count(1)):
        numberids[i, j] = k

    zmax = max(rows * cols - int(np.sum(problem)), int(np.max(problem))) - 1

    model = cp_model.CpModel()

    # Declare variables
    x = [{} for i in range(nnumbers+1)]
    r = {}
    z = {}
    v = {}
    h = {}

    for i, j in np.ndindex(rows, cols):
        variables = []
        for k in range(nnumbers + 1):
            x[k][i, j] = model.NewBoolVar(f'x_{i}_{j}_{k}')
            if problem[i, j] != 0 and k != numberids[i, j]:
                variables.append(x[k][i, j].Not())
            else:
                variables.append(x[k][i, j])
        if problem[i, j] != 0:
            model.AddBoolAnd(variables)
        else:
            model.Add(sum(variables) == 1)

        r[i, j] = model.NewBoolVar(f'r_{i}_{j}')
        z[i, j] = model.NewIntVar(0, zmax, f'z_{i}_{j}')

        if i < rows - 1:
            v[i, j] = model.NewBoolVar(f'v_{i}_{j}')
        if j < cols - 1:
            h[i, j] = model.NewBoolVar(f'h_{i}_{j}')

    # Constraint: Number of white cells
    for idx, k in numberids.items():
        model.Add(sum(x[k].values()) == problem[idx])

    # Constraint: Black cells cannot form 2x2 squares
    for i, j in np.ndindex(rows-1, cols-1):
        model.AddBoolOr([
            x[0][i, j].Not(), x[0][i, j+1].Not(),
            x[0][i+1, j].Not(), x[0][i+1, j+1].Not(),
        ])

    # Constraint: Connectivity of islands and black cells
    for i, j in np.ndindex(rows-1, cols):
        for k in range(nnumbers + 1):
            model.AddBoolOr([v[i, j].Not(), x[k][i, j].Not(), x[k][i+1, j]])
            model.AddBoolOr([v[i, j].Not(), x[k][i, j], x[k][i+1, j].Not()])
        model.AddBoolOr([v[i, j], x[0][i, j], x[0][i+1, j]])
        model.AddBoolOr([v[i, j], x[0][i, j].Not(), x[0][i+1, j].Not()])

    for i, j in np.ndindex(rows, cols-1):
        for k in range(nnumbers + 1):
            model.AddBoolOr([h[i, j].Not(), x[k][i, j].Not(), x[k][i, j+1]])
            model.AddBoolOr([h[i, j].Not(), x[k][i, j], x[k][i, j+1].Not()])
        model.AddBoolOr([h[i, j], x[0][i, j], x[0][i, j+1]])
        model.AddBoolOr([h[i, j], x[0][i, j].Not(), x[0][i, j+1].Not()])

    model.Add(sum(r.values()) == 1)

    for i, j in np.ndindex(rows, cols):
        model.AddImplication(x[0][i, j].Not(), r[i, j].Not())

        if problem[i, j] != 0:
            model.Add(z[i, j] == 0)
            continue
        else:
            model.Add(z[i, j] == 0).OnlyEnforceIf(r[i, j])
            model.Add(z[i, j] != 0).OnlyEnforceIf(r[i, j].Not())

        variables = []
        if i > 0:
            var = model.NewBoolVar('')
            model.AddImplication(var, v[i-1, j])
            model.Add(z[i, j] > z[i-1, j]).OnlyEnforceIf(var)
            variables.append(var)
        if i < rows - 1:
            var = model.NewBoolVar('')
            model.AddImplication(var, v[i, j])
            model.Add(z[i, j] > z[i+1, j]).OnlyEnforceIf(var)
            variables.append(var)
        if j > 0:
            var = model.NewBoolVar('')
            model.AddImplication(var, h[i, j-1])
            model.Add(z[i, j] > z[i, j-1]).OnlyEnforceIf(var)
            variables.append(var)
        if j < cols - 1:
            var = model.NewBoolVar('')
            model.AddImplication(var, h[i, j])
            model.Add(z[i, j] > z[i, j+1]).OnlyEnforceIf(var)
            variables.append(var)
        model.AddBoolOr(variables).OnlyEnforceIf(r[i, j].Not())

    # Optimization
    for (ii, jj), k in numberids.items():
        variables = []
        for i, j in np.ndindex(rows, cols):
            if problem[i, j] != 0:
                continue
            dist = abs(i - ii) + abs(j - jj)
            if dist >= problem[ii, jj]:
                variables.append(x[k][i, j].Not())
        model.AddBoolAnd(variables)

    roots = []
    for i, j in np.ndindex(rows, cols):
        if problem[i, j] == 1:
            variables = []
            if i > 0:
                variables.append(x[0][i-1, j])
                roots.append([r[i-1, j]])
            if i < rows - 1:
                variables.append(x[0][i+1, j])
                roots.append([r[i+1, j]])
            if j > 0:
                variables.append(x[0][i, j-1])
                roots.append([r[i, j-1]])
            if j < cols - 1:
                variables.append(x[0][i, j+1])
                roots.append([r[i, j+1]])
            model.AddBoolAnd(variables)
        elif problem[i, j] != 0:
            neighbors = []
            if i > 0:
                neighbors.append(r[i-1, j])
            if i < rows - 1:
                neighbors.append(r[i+1, j])
            if j > 0:
                neighbors.append(r[i, j-1])
            if j < cols - 1:
                neighbors.append(r[i, j+1])
            if len(neighbors) >= problem[i, j]:
                roots.append(neighbors)
        else:
            neighbors = 0
            if i > 0 and problem[i-1, j] != 0:
                neighbors += 1
            if i < rows - 1 and problem[i+1, j] != 0:
                neighbors += 1
            if j > 0 and problem[i, j-1] != 0:
                neighbors += 1
            if j < cols - 1 and problem[i, j+1] != 0:
                neighbors += 1
            if neighbors >= 2:
                model.AddBoolAnd([x[0][i, j]])
                if not roots:
                    roots.append([r[i, j]])

    if roots:
        model.AddBoolOr(min(roots, key=len))

    # Solving
    solver = cp_model.CpSolver()
    if timeout is not None:
        solver.parameters.max_time_in_seconds = timeout
    callback = SolutionCallback(problem, x[0], outputter)
    status = solver.SolveWithSolutionCallback(model, callback)
    time = solver.WallTime()

    if find_all:
        while status == cp_model.FEASIBLE:
            model.AddBoolOr([
                it.Not() if solver.BooleanValue(it) else it
                for it in x[0].values()
            ])
            status = solver.SolveWithSolutionCallback(model, callback)
            time += solver.WallTime()
            if status == cp_model.INFEASIBLE:
                status = cp_model.OPTIMAL
            elif status == cp_model.UNKNOWN:
                status = cp_model.FEASIBLE
                break

    with redirect_stdout(sys.stderr):
        print('Status:', solver.StatusName(status))
        if find_all:
            print('Number of solutions:', callback.number_of_solutions())
        print('Wall time:', time, 's')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
            usage='%(prog)s [-a | -o FILE] <URL or file>',
            epilog=textwrap.dedent('''\
            Pzprv3 Editor:
              http://pzv.jp/p.html?nurikabe_edit
            '''))
    options = parser.add_mutually_exclusive_group()
    options.add_argument('-a', '--all', action='store_true',
                         help='Find all possible solutions')
    options.add_argument('-o', '--output', dest='dest',
                         help='Output pzvrv3 file')
    parser.add_argument('-t', '--timeout', type=float,
                        help='Timeout in seconds')
    parser.add_argument('data', nargs='?', help='Pzprv3 URL or file')
    args = parser.parse_args()

    if args.data is None:
        parser.print_help()
        parser.exit()

    m = re_pzprurl.fullmatch(args.data)
    if m is not None:
        problem = load_pzprurl(*m.groups())
    else:
        with open(args.data) as f:
            problem = load_pzprv3file(f)
    if args.dest is None:
        outputter = SolutionPrinter
    else:
        outputter = Pzprv3FileOutputter(args.dest)
    solve(problem, outputter, find_all=args.all, timeout=args.timeout)
