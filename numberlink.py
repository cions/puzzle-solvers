#!/usr/bin/python

import argparse
import collections
import itertools
import re
import sys
import textwrap
from contextlib import nullcontext, redirect_stdout

import numpy as np
from ortools.sat.python import cp_model


Solution = collections.namedtuple('Solution', ('vlines', 'hlines'))
re_pzprurl = re.compile(
    r'(?:https?://pzv\.jp/p\.html\?)?numlin/(\d+)/(\d+)/(.+)', re.ASCII)


def load_pzprurl(cols, rows, data):
    cols = int(cols)
    rows = int(rows)

    problem = np.full((rows, cols), -1, dtype='int64').reshape(-1)
    i = 0
    number = 0
    left = 1
    offset = 0
    for x in data:
        if x in '0123456789abcdef':
            number = (number << 4) | int(x, 16)
            left -= 1
            if left <= 0:
                problem[i] = number + offset
                i += 1
                number = 0
                left = 1
                offset = 0
        elif x == '-':
            left = 2
            offset = 0
        elif x == '+':
            left = 3
            offset = 0
        elif x == '=':
            left = 3
            offset = 4096
        elif x == '%':
            left = 3
            offset = 8192
        elif x == '*':
            left = 4
            offset = 12240
        elif x == '$':
            left = 5
            offset = 77776
        elif x.islower():
            i += int(x, 36) - 15

    return problem.reshape((rows, cols))


def load_pzprv3file(f):
    if f.readline() != 'pzprv3\n':
        raise ValueError('invalid format')
    if f.readline() != 'numlin\n':
        raise ValueError('invalid format')

    rows = int(f.readline())
    cols = int(f.readline())

    problem = np.full((rows, cols), -1, dtype='int64')
    for i in range(rows):
        row = f.readline().split()
        for j, x in enumerate(row):
            if x == '.':
                continue
            problem[i, j] = int(x)

    return problem


class Pzprv3FileOutputter:
    def __init__(self, dest):
        self.dest = dest

    def __call__(self, problem, solution):
        if self.dest != '-':
            cm = open(self.dest, 'w')
        else:
            cm = nullcontext(sys.stdout)
        with cm as f, redirect_stdout(f):
            print('pzprv3')
            print('numlin')
            print(problem.shape[0])
            print(problem.shape[1])

            for row in np.where(problem >= 0, problem, '.'):
                print(' '.join(row))

            for row in np.where(solution.hlines, '1', '0'):
                print(' '.join(row))

            for row in np.where(solution.vlines, '1', '0'):
                print(' '.join(row))


def SolutionPrinter(problem, solution):
    rows, cols = problem.shape
    v, h = solution
    w = len(str(problem.max()))

    for i in range(rows):
        for j in range(cols):
            lines = 0
            lines |= 1 if i > 0 and v[i-1, j] else 0
            lines |= 2 if j > 0 and h[i, j-1] else 0
            lines |= 4 if i < rows - 1 and v[i, j] else 0
            lines |= 8 if j < cols - 1 and h[i, j] else 0

            if problem[i, j] >= 0:
                s = str(problem[i, j])
            elif lines == 3:
                s = '┘'
            elif lines == 5:
                s = '│'
            elif lines == 6:
                s = '┐'
            elif lines == 9:
                s = '└'
            elif lines == 10:
                s = '─'
            elif lines == 12:
                s = '┌'
            else:
                s = ' '
            if len(s) < w:
                rpad = (w - len(s)) // 2
                lpad = (w - len(s) - rpad)
                s = ('─' if lines & 2 else ' ') * lpad + s
                s = s + ('─' if lines & 8 else ' ') * rpad
            print(s, end='─' if lines & 8 else ' ')
        print()
    print()


class SolutionCallback(cp_model.CpSolverSolutionCallback):
    def __init__(self, problem, vvars, hvars, outputter):
        super().__init__()
        self.problem = problem
        self.vvars = vvars
        self.hvars = hvars
        self.outputter = outputter
        self.nsolutions = 0

    def on_solution_callback(self):
        self.nsolutions += 1

        rows, cols = self.problem.shape

        vlines = np.empty((rows-1, cols), dtype=bool)
        for i, j in np.ndindex(vlines.shape):
            vlines[i, j] = self.BooleanValue(self.vvars[i, j])
        hlines = np.empty((rows, cols-1), dtype=bool)
        for i, j in np.ndindex(hlines.shape):
            hlines[i, j] = self.BooleanValue(self.hvars[i, j])

        solution = Solution(vlines, hlines)
        self.outputter(self.problem, solution)

    def number_of_solutions(self):
        return self.nsolutions


def solve(problem, outputter, find_alt=0, timeout=None):
    rows, cols = problem.shape
    xmin = int(problem.min()) - int(find_alt > 0)
    xmax = int(problem.max())

    model = cp_model.CpModel()

    # Declare variables
    v = {}   # vertical line
    h = {}   # horizontal line
    x = {}   # node number
    if find_alt == 1:
        b = {}   # node degree is zero
        s = {}   # node is not turning

    for i, j in np.ndindex(rows-1, cols):
        v[i, j] = model.NewBoolVar(f'v_{i}_{j}')

    for i, j in np.ndindex(rows, cols-1):
        h[i, j] = model.NewBoolVar(f'h_{i}_{j}')

    for i, j in np.ndindex(rows, cols):
        x[i, j] = model.NewIntVar(xmin, xmax, f'x_{i}_{j}')
        if find_alt == 1:
            b[i, j] = model.NewBoolVar(f'b_{i}_{j}')
            s[i, j] = model.NewBoolVar(f's_{i}_{j}')

    # Constraints
    for i, j in np.ndindex(rows, cols):
        neighbors = []

        if i > 0:
            neighbors.append(v[i-1, j])
        if i < rows - 1:
            neighbors.append(v[i, j])
        if j > 0:
            neighbors.append(h[i, j-1])
        if j < cols - 1:
            neighbors.append(h[i, j])

        if problem[i, j] >= 0:
            model.Add(sum(neighbors) == 1)
            if find_alt == 1:
                model.AddBoolAnd([b[i, j].Not(), s[i, j].Not()])
        elif find_alt == 0:
            model.Add(sum(neighbors) == 2)
        elif find_alt == 1:
            model.Add(sum(neighbors) == 2).OnlyEnforceIf(b[i, j].Not())
            model.Add(sum(neighbors) == 0).OnlyEnforceIf(b[i, j])
            model.Add(x[i, j] == xmin).OnlyEnforceIf(b[i, j])

            if 0 < i < rows - 1 and 0 < j < cols - 1:
                model.Add(v[i-1, j] == v[i, j]).OnlyEnforceIf(s[i, j])
                model.Add(h[i, j-1] == h[i, j]).OnlyEnforceIf(s[i, j])
            elif 0 < i < rows - 1:
                model.AddImplication(s[i, j], h[i, j-1 if j > 0 else j].Not())
            elif 0 < j < cols - 1:
                model.AddImplication(s[i, j], v[i-1 if i > 0 else i, j].Not())
        else:
            model.Add(sum(neighbors) <= 2)
            model.Add(sum(neighbors) != 1)
            model.Add(x[i, j] == xmin).OnlyEnforceIf([x.Not() for x in neighbors])

    for i, j in zip(*np.where(problem >= 0)):
        model.Add(x[i, j] == problem[i, j])

    for i, j in np.ndindex(rows-1, cols):
        model.Add(x[i, j] == x[i+1, j]).OnlyEnforceIf(v[i, j])

    for i, j in np.ndindex(rows, cols-1):
        model.Add(x[i, j] == x[i, j+1]).OnlyEnforceIf(h[i, j])

    # Optimization: Eliminate patterns that not appears in the unique
    #               solution
    if find_alt != 2:
        for i, j in np.ndindex(rows-1, cols-1):
            model.AddBoolOr([h[i, j].Not(), v[i, j].Not(), h[i+1, j].Not()])
            model.AddBoolOr([h[i, j].Not(), v[i, j+1].Not(), h[i+1, j].Not()])
            model.AddBoolOr([h[i, j].Not(), v[i, j].Not(), v[i, j+1].Not()])
            model.AddBoolOr([h[i+1, j].Not(), v[i, j].Not(), v[i, j+1].Not()])

    # Set objective
    if find_alt == 1:
        model.Maximize(sum(itertools.chain(b.values(), s.values())))

    # Solving
    solver = cp_model.CpSolver()
    if timeout is not None:
        solver.parameters.max_time_in_seconds = timeout
    callback = SolutionCallback(problem, v, h, outputter)
    if find_alt == 2:
        status = solver.SearchForAllSolutions(model, callback)
    else:
        status = solver.SolveWithSolutionCallback(model, callback)

    with redirect_stdout(sys.stderr):
        print('Status:', solver.StatusName(status))
        if find_alt == 2:
            print('Number of solutions:', callback.number_of_solutions())
        print('Wall time:', solver.WallTime(), 's')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
            usage='%(prog)s [-a | [-s] [-o FILE]] <URL or file>',
            epilog=textwrap.dedent('''\
            Pzprv3 Editor:
              http://pzv.jp/p.html?numlin_edit
            '''))
    parser.set_defaults(find_alt=0)
    options = parser.add_mutually_exclusive_group()
    options.add_argument('-a', '--all', action='store_const',
                         dest='find_alt', const=2,
                         help='Find all possible solutions')
    options.add_argument('-s', '--short', action='store_const',
                         dest='find_alt', const=1,
                         help='Find short-circuit solution')
    parser.add_argument('-o', '--output', dest='dest',
                        help='Output pzvrv3 file')
    parser.add_argument('-t', '--timeout', type=float,
                        help='Timeout in seconds')
    parser.add_argument('data', nargs='?', help='Pzprv3 URL or file')
    args = parser.parse_args()

    if args.data is None:
        parser.print_help()
        parser.exit()

    if args.find_alt == 2 and args.dest:
        parser.print_usage()
        parser.exit(2, 'error: -a and -o are mutually exclusive\n')

    m = re_pzprurl.fullmatch(args.data)
    if m is not None:
        problem = load_pzprurl(*m.groups())
    else:
        with open(args.data) as f:
            problem = load_pzprv3file(f)
    if args.dest is None:
        outputter = SolutionPrinter
    else:
        outputter = Pzprv3FileOutputter(args.dest)
    solve(problem, outputter, find_alt=args.find_alt, timeout=args.timeout)
