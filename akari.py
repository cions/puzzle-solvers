#!/usr/bin/python

import argparse
import re
import sys
import textwrap
from contextlib import nullcontext, redirect_stdout

import numpy as np
from ortools.sat.python import cp_model


re_pzprurl = re.compile(
    r'(?:https?://pzv\.jp/p\.html\?)?akari/(\d+)/(\d+)/(.+)', re.ASCII)


def load_pzprurl(cols, rows, data):
    cols = int(cols)
    rows = int(rows)

    problem = np.full((rows, cols), -1, dtype='int8').reshape(-1)
    i = 0
    for x in data:
        if x in '01234':
            problem[i] = int(x, 16)
        elif x in '56789':
            problem[i] = int(x, 16) - 5
            i += 1
        elif x in 'abcde':
            problem[i] = int(x, 16) - 10
            i += 2
        elif x == '.':
            problem[i] = 5
        else:
            i += int(x, 36) - 16
        i += 1

    return problem.reshape((rows, cols))


def load_pzprv3file(f):
    if f.readline() != 'pzprv3\n':
        raise ValueError('invalid format')
    if f.readline() != 'lightup\n':
        raise ValueError('invalid format')

    rows = int(f.readline())
    cols = int(f.readline())

    problem = np.full((rows, cols), -1, dtype='int8')

    for i in range(rows):
        row = f.readline().split()
        for j, x in enumerate(row):
            if x in ('.', '#', '+'):
                continue
            if x == '-':
                problem[i, j] = 5
            else:
                problem[i, j] = int(x)

    return problem


class Pzprv3FileOutputter:
    def __init__(self, dest):
        self.dest = dest

    def __call__(self, problem, solution):
        if self.dest != '-':
            cm = open(self.dest, 'w')
        else:
            cm = nullcontext(sys.stdout)
        with cm as f, redirect_stdout(f):
            print('pzprv3')
            print('lightup')
            print(problem.shape[0])
            print(problem.shape[1])

            board = np.where(solution, '#', '.')
            board = np.where(problem < 0, board, problem)
            board[problem == 5] = '-'
            for row in board:
                print(' '.join(str(x) for x in row))


def SolutionPrinter(problem, solution):
    board = np.where(solution, '@', ' ')
    board = np.where(problem < 0, board, problem)
    board[problem == 5] = '#'
    for row in board:
        print(' '.join(row))
    print()


class SolutionCallback(cp_model.CpSolverSolutionCallback):
    def __init__(self, problem, variables, outputter):
        super().__init__()
        self.problem = problem
        self.variables = variables
        self.outputter = outputter
        self.nsolutions = 0

    def on_solution_callback(self):
        self.nsolutions += 1

        solution = np.zeros_like(self.problem, dtype='bool')
        for k, v in self.variables.items():
            solution[k] = self.BooleanValue(v)
        self.outputter(self.problem, solution)

    def number_of_solutions(self):
        return self.nsolutions


def solve(problem, outputter, find_all=False, timeout=None):
    rows, cols = problem.shape

    model = cp_model.CpModel()

    # Declare variables
    x = {}
    for i, j in np.ndindex(rows, cols):
        if problem[i, j] >= 0:
            continue
        x[i, j] = model.NewBoolVar(f'x_{i}_{j}')

    # Constraint: Number indicating how many light bulbs is placed around it
    for i, j in zip(*np.where(problem >= 0)):
        if problem[i, j] == 5:
            continue
        variables = []
        if i > 0 and problem[i-1, j] < 0:
            variables.append(x[i-1, j])
        if i < rows - 1 and problem[i+1, j] < 0:
            variables.append(x[i+1, j])
        if j > 0 and problem[i, j-1] < 0:
            variables.append(x[i, j-1])
        if j < cols - 1 and problem[i, j+1] < 0:
            variables.append(x[i, j+1])
        model.Add(sum(variables) == problem[i, j])

    # Constraint: No light bulbs shine each other
    for i in range(rows):
        variables = []
        for j, cell in enumerate(problem[i]):
            if cell < 0:
                variables.append(x[i, j])
                continue
            model.Add(sum(variables) <= 1)
            variables.clear()
        model.Add(sum(variables) <= 1)

    for j in range(cols):
        variables = []
        for i, cell in enumerate(problem[:, j]):
            if cell < 0:
                variables.append(x[i, j])
                continue
            model.Add(sum(variables) <= 1)
            variables.clear()
        model.Add(sum(variables) <= 1)

    # Constraint: All white cells must be lit up
    for i, j in np.ndindex(rows, cols):
        if problem[i, j] >= 0:
            continue
        variables = [x[i, j]]

        k = i - 1
        while k >= 0 and problem[k, j] < 0:
            variables.append(x[k, j])
            k -= 1

        k = i + 1
        while k < rows and problem[k, j] < 0:
            variables.append(x[k, j])
            k += 1

        k = j - 1
        while k >= 0 and problem[i, k] < 0:
            variables.append(x[i, k])
            k -= 1

        k = j + 1
        while k < cols and problem[i, k] < 0:
            variables.append(x[i, k])
            k += 1

        model.AddBoolOr(variables)

    # Solving
    solver = cp_model.CpSolver()
    if timeout is not None:
        solver.parameters.max_time_in_seconds = timeout
    callback = SolutionCallback(problem, x, outputter)
    if find_all:
        status = solver.SearchForAllSolutions(model, callback)
    else:
        status = solver.SolveWithSolutionCallback(model, callback)

    with redirect_stdout(sys.stderr):
        print('Status:', solver.StatusName(status))
        if find_all:
            print('Number of solutions:', callback.number_of_solutions())
        print('Wall time:', solver.WallTime(), 's')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
            usage='%(prog)s [-a | -o FILE] <URL or file>',
            epilog=textwrap.dedent('''\
            Pzprv3 Editor:
              http://pzv.jp/p.html?akari_edit
            '''))
    options = parser.add_mutually_exclusive_group()
    options.add_argument('-a', '--all', action='store_true',
                         help='Find all possible solutions')
    options.add_argument('-o', '--output', dest='dest',
                         help='Output pzvrv3 file')
    parser.add_argument('-t', '--timeout', type=float,
                        help='Timeout in seconds')
    parser.add_argument('data', nargs='?', help='Pzprv3 URL or file')
    args = parser.parse_args()

    if args.data is None:
        parser.print_help()
        parser.exit()

    m = re_pzprurl.fullmatch(args.data)
    if m is not None:
        problem = load_pzprurl(*m.groups())
    else:
        with open(args.data) as f:
            problem = load_pzprv3file(f)
    if args.dest is None:
        outputter = SolutionPrinter
    else:
        outputter = Pzprv3FileOutputter(args.dest)
    solve(problem, outputter, find_all=args.all, timeout=args.timeout)
