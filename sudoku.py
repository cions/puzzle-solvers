#!/usr/bin/python

import argparse
import re
import sys
import textwrap
from contextlib import nullcontext, redirect_stdout
from itertools import product

import numpy as np
from ortools.sat.python import cp_model


re_pzprurl = re.compile(
    r'(?:https?://pzv\.jp/p\.html\?)?sudoku/(\d+)/(\d+)/(.+)', re.ASCII)


def load_pzprurl(cols, rows, data):
    cols = int(cols)
    rows = int(rows)

    problem = np.zeros((rows, cols), dtype='uint64').reshape(-1)

    i = 0
    number = 0
    left = 1
    offset = 0
    for x in data:
        if x in '0123456789abcdef':
            number = (number << 4) | int(x, 16)
            left -= 1
            if left <= 0:
                problem[i] = number + offset
                i += 1
                number = 0
                left = 1
                offset = 0
        elif x == '-':
            left = 2
            offset = 0
        elif x == '+':
            left = 3
            offset = 0
        elif x == '=':
            left = 3
            offset = 4096
        elif x == '%':
            left = 3
            offset = 8192
        elif x == '*':
            left = 4
            offset = 12240
        elif x == '$':
            left = 5
            offset = 77776
        elif x.islower():
            i += int(x, 36) - 15

    return problem.reshape((rows, cols))


def load_pzprv3file(f):
    if f.readline() != 'pzprv3\n':
        raise ValueError('invalid format')
    if f.readline() != 'sudoku\n':
        raise ValueError('invalid format')

    size = int(f.readline())

    problem = np.zeros((size, size), dtype='uint64')

    for i in range(size):
        row = f.readline().split()
        for j, x in enumerate(row):
            if x == '.':
                continue
            problem[i, j] = int(x)

    return problem


class Pzprv3FileOutputter:
    def __init__(self, dest):
        self.dest = dest

    def __call__(self, problem, solution):
        if self.dest != '-':
            cm = open(self.dest, 'w')
        else:
            cm = nullcontext(sys.stdout)
        with cm as f, redirect_stdout(f):
            print('pzprv3')
            print('sudoku')
            print(problem.shape[0])
            for row in np.where(problem != 0, problem, '.'):
                print(' '.join(row))
            for row in np.where(problem == 0, solution, '.'):
                print(' '.join(row))


def SolutionPrinter(problem, solution):
    n = solution.shape[0]
    m = int(n ** 0.5)
    w = len(str(n))

    hline = '+'
    for j in range(m):
        hline += '-' * ((w + 1) * m + 1) + '+'

    for i in range(n):
        if i % m == 0:
            print(hline)
        for j in range(n):
            if j % m == 0:
                print('| ', end='')
            print(str(solution[i, j]).rjust(w), end=' ')
        print('|')
    print(hline)
    print()


class SolutionCallback(cp_model.CpSolverSolutionCallback):
    def __init__(self, problem, variables, outputter):
        super().__init__()
        self.problem = problem
        self.variables = variables
        self.outputter = outputter
        self.nsolutions = 0

    def on_solution_callback(self):
        self.nsolutions += 1

        solution = np.empty(self.problem.shape, dtype='uint64')
        for i, j in np.ndindex(self.problem.shape):
            solution[i, j] = self.Value(self.variables[i, j])
        self.outputter(self.problem, solution)

    def number_of_solutions(self):
        return self.nsolutions


def solve(problem, outputter, find_all=False):
    model = cp_model.CpModel()

    n = problem.shape[0]
    m = int(n ** 0.5)

    x = {}
    for i, j in product(range(n), range(n)):
        x[i, j] = model.NewIntVar(1, n, f'x_{i}_{j}')

    for i in range(n):
        model.AddAllDifferent(x[i, j] for j in range(n))

    for j in range(n):
        model.AddAllDifferent(x[i, j] for i in range(n))

    boxes = [
        [(ii + i, jj + j) for (i, j) in product(range(m), range(m))]
        for (ii, jj) in product(range(0, n, m), range(0, n, m))
    ]
    for box in boxes:
        model.AddAllDifferent(x[i, j] for i, j in box)

    for i, j in zip(*np.where(problem != 0)):
        model.Add(x[i, j] == problem[i, j])

    # Solve
    solver = cp_model.CpSolver()
    callback = SolutionCallback(problem, x, outputter)
    if find_all:
        status = solver.SearchForAllSolutions(model, callback)
    else:
        status = solver.SolveWithSolutionCallback(model, callback)

    with redirect_stdout(sys.stderr):
        print('Status:', solver.StatusName(status))
        if find_all:
            print('Number of solutions:', callback.number_of_solutions())
        print('Wall time:', solver.WallTime(), 's')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
            usage='%(prog)s [-a | -o FILE] <URL or file>',
            epilog=textwrap.dedent('''\
            Pzprv3 Editor:
              http://pzv.jp/p.html?sudoku_edit
            '''))
    options = parser.add_mutually_exclusive_group()
    options.add_argument('-a', '--all', action='store_true',
                         help='Find all possible solutions')
    options.add_argument('-o', '--output', dest='dest',
                         help='Output pzvrv3 file')
    parser.add_argument('data', nargs='?', help='Pzprv3 URL or file')
    args = parser.parse_args()

    if args.data is None:
        parser.print_help()
        parser.exit()

    m = re_pzprurl.fullmatch(args.data)
    if m is not None:
        problem = load_pzprurl(*m.groups())
    else:
        with open(args.data) as f:
            problem = load_pzprv3file(f)
    if args.dest is None:
        outputter = SolutionPrinter
    else:
        outputter = Pzprv3FileOutputter(args.dest)
    solve(problem, outputter, find_all=args.all)
