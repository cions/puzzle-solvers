#!/usr/bin/python

import sys

from ortools.sat.python import cp_model


class SolutionCallback(cp_model.CpSolverSolutionCallback):
    def __init__(self, variables):
        super().__init__()
        self.variables = variables
        self.nsolutions = 0

    def on_solution_callback(self):
        self.nsolutions += 1

        if len(self.variables) > 12:
            return
        if len(self.variables) > 9:
            print([self.Value(v) for v in self.variables])
            return

        for v in self.variables:
            i = self.Value(v)
            for j in range(len(self.variables)):
                print('#' if i == j else '.', end=' ')
            print()
        print()

    def number_of_solutions(self):
        return self.nsolutions


def solve(size):
    model = cp_model.CpModel()

    x, d, u = [], [], []
    for i in range(size):
        x.append(model.NewIntVar(0, size - 1, f'x_{i}'))

        d.append(model.NewIntVar(1 - size, size - 1, f'd_{i}'))
        model.Add(x[i] - i == d[i])

        u.append(model.NewIntVar(0, 2 * size - 2, f'u_{i}'))
        model.Add(x[i] + i == u[i])

    model.AddAllDifferent(x)
    model.AddAllDifferent(d)
    model.AddAllDifferent(u)

    solver = cp_model.CpSolver()
    callback = SolutionCallback(x)
    status = solver.SearchForAllSolutions(model, callback)

    print('Status:', solver.StatusName(status))
    print('Number of solutions:', callback.number_of_solutions())
    print('Wall time:', solver.WallTime(), 's')


if __name__ == '__main__':
    solve(int(sys.argv[1]) if len(sys.argv) > 1 else 8)
