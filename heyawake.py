#!/usr/bin/python

import argparse
import collections
import itertools
import re
import sys
import textwrap
from contextlib import nullcontext, redirect_stdout

import networkx as nx
import numpy as np
from ortools.sat.python import cp_model


Problem = collections.namedtuple('Problem', ('board', 'npainted', 'ayeheya'))
re_pzprurl = re.compile(
    r'(?:https?://pzv\.jp/p\.html\?)?(heyawake|ayeheya)/(\d+)/(\d+)/(.+)',
    re.ASCII)


def load_pzprurl(variant, cols, rows, data):
    cols = int(cols)
    rows = int(rows)

    G = nx.Graph()

    pos1 = ((cols - 1) * rows + 4) // 5
    pos2 = (cols * (rows - 1) + 4) // 5 + pos1

    vborders = int(data[:pos1], 32) >> (-(cols - 1) * rows % 5)
    hborders = int(data[pos1:pos2], 32) >> (-cols * (rows - 1) % 5)

    vbitmask = 1 << ((cols - 1) * rows)
    hbitmask = 1 << (cols * (rows - 1))
    for i, j in np.ndindex(rows, cols):
        G.add_node((i, j))
        if j > 0:
            vbitmask >>= 1
        if i > 0:
            hbitmask >>= 1
        if j > 0 and vborders & vbitmask == 0:
            G.add_edge((i, j-1), (i, j))
        if i > 0 and hborders & hbitmask == 0:
            G.add_edge((i-1, j), (i, j))

    board = np.empty((rows, cols), dtype='int64')
    components = sorted(nx.connected_components(G), key=min)
    for i, nodes in enumerate(components):
        for node in nodes:
            board[node] = i

    npainted = {}
    room = 0
    number = 0
    left = 1
    offset = 0
    for x in data[pos2:]:
        if x in '0123456789abcdef':
            number = (number << 4) | int(x, 16)
            left -= 1
            if left <= 0:
                npainted[room] = number + offset
                room += 1
                number = 0
                left = 1
                offset = 0
        elif x == '-':
            left = 2
            offset = 0
        elif x == '+':
            left = 3
            offset = 0
        elif x == '=':
            left = 3
            offset = 4096
        elif x == '%':
            left = 3
            offset = 8192
        elif x == '*':
            left = 4
            offset = 12240
        elif x == '$':
            left = 5
            offset = 77776
        elif x.islower():
            room += int(x, 36) - 15

    return Problem(board, npainted, variant == 'ayeheya')


def load_pzprv3file(f):
    if f.readline() != 'pzprv3\n':
        raise ValueError('invalid format')
    variant = f.readline().rstrip('\n')
    if variant not in ('heyawake', 'ayeheya'):
        raise ValueError('invalid format')

    rows = int(f.readline())
    cols = int(f.readline())
    _ = f.readline()  # number of rooms

    room_map = {}
    board = np.empty((rows, cols), dtype='int64')
    npainted = {}

    for i in range(rows):
        row = f.readline().split()
        for j, x in enumerate(row):
            board[i, j] = room_map.setdefault(x, len(room_map))

    for i in range(rows):
        row = f.readline().split()
        for j, x in enumerate(row):
            if x == '.':
                continue
            npainted[board[i, j]] = int(x)

    return Problem(board, npainted, variant == 'ayeheya')


class Pzprv3FileOutputter:
    def __init__(self, dest):
        self.dest = dest

    def __call__(self, problem, solution):
        if self.dest != '-':
            cm = open(self.dest, 'w')
        else:
            cm = nullcontext(sys.stdout)
        with cm as f, redirect_stdout(f):
            print('pzprv3')
            print('ayeheya' if problem.ayeheya else 'heyawake')
            print(problem.board.shape[0])
            print(problem.board.shape[1])
            print(problem.board.max() + 1)

            for row in problem.board:
                print(' '.join(str(x) for x in row))

            numbers = np.full(problem.board.size, -1, dtype='int64')
            _, indices = np.unique(problem.board, return_index=True)
            for room, number in problem.npainted.items():
                numbers[indices[room]] = number
            numbers = numbers.reshape(problem.board.shape)
            for row in np.where(numbers >= 0, numbers, '.'):
                print(' '.join(row))

            for row in np.where(solution, '#', '+'):
                print(' '.join(row))


def SolutionPrinter(problem, solution):
    for row in np.where(solution, '#', '.'):
        print(' '.join(row))
    print()


class SolutionCallback(cp_model.CpSolverSolutionCallback):
    def __init__(self, problem, variables, outputter):
        super().__init__()
        self.problem = problem
        self.variables = variables
        self.outputter = outputter
        self.nsolutions = 0

    def on_solution_callback(self):
        self.nsolutions += 1

        solution = np.empty_like(self.problem.board, dtype='bool')
        for k, v in self.variables.items():
            solution[k] = self.BooleanValue(v)
        self.outputter(self.problem, solution)

    def number_of_solutions(self):
        return self.nsolutions


def solve(problem, outputter, find_all=False, timeout=None):
    board, npainted, ayeheya = problem
    rows, cols = board.shape

    model = cp_model.CpModel()

    # Declare variables
    x, r, z = {}, {}, {}
    for i, j in np.ndindex(rows, cols):
        x[i, j] = model.NewBoolVar(f'x_{i}_{j}')
        r[i, j] = model.NewBoolVar(f'r_{i}_{j}')
        z[i, j] = model.NewIntVar(0, board.size, f'z_{i}_{j}')

    # Constraint: Number of painted cells in a room
    for room, number in npainted.items():
        model.Add(sum(x[i] for i in zip(*np.where(board == room))) == number)

    # Constraint: Painted cells cannot be adjacent orthogonally
    for i, j in np.ndindex(rows, cols):
        if i < rows - 1:
            model.AddBoolOr([x[i, j].Not(), x[i+1, j].Not()])
        if j < cols - 1:
            model.AddBoolOr([x[i, j].Not(), x[i, j+1].Not()])

    # Constraint: White cells cannot stretch across more than two rooms
    #             in a straight line
    for i in range(rows):
        indices = np.flatnonzero(np.diff(board[i])) + 1
        for start, end in zip(indices, indices[1:]):
            model.AddBoolOr([x[i, j] for j in range(start - 1, end + 1)])

    for j in range(cols):
        indices = np.flatnonzero(np.diff(board[:, j])) + 1
        for start, end in zip(indices, indices[1:]):
            model.AddBoolOr([x[i, j] for i in range(start - 1, end + 1)])

    # Constraint: Each room must be point symmetric (ayeheya only)
    if ayeheya:
        for room in np.unique(board):
            ii, jj = np.where(board == room)
            ir = ii.max() - (ii - ii.min())
            jr = jj.max() - (jj - jj.min())
            for i1, j1, i2, j2 in zip(ii, jj, ir, jr):
                if not (i1 < i2 or i1 == i2 and j1 < j2):
                    break
                model.Add(x[i1, j1] == x[i2, j2])

    # Optimizations for Performance
    for room, number in npainted.items():
        if number == 4:
            ii, jj = np.where(board == room)
            h, w = ii.max() - ii.min() + 1, jj.max() - jj.min() + 1
            if h * w != ii.size:
                continue
            if h != w or h != 3:
                continue
            i, j = int(ii.min()), int(jj.min())
            model.AddBoolAnd([
                x[i, j+1].Not(), x[i+1, j].Not(),
                x[i+1, j+2].Not(), x[i+2, j+1].Not()
            ])
        elif number >= 5 and (3 * number + 1) & (3 * number) == 0:
            ii, jj = np.where(board == room)
            h, w = ii.max() - ii.min() + 1, jj.max() - jj.min() + 1
            if h * w != ii.size:
                continue
            if h != w or h != int((3 * number + 1) ** 0.5) - 1:
                continue
            i, j = int(ii.min()), int(jj.min())
            s = 0
            d = 2
            variables = []
            while s < w:
                for di, dj in itertools.product(range(s, h, d), repeat=2):
                    variables.append(x[i+di, j+dj])
                s = (s * 2) + 1
                d *= 2
            model.AddBoolAnd(variables)

    # Constraint: White cells must not be separated by black cells
    model.Add(sum(r.values()) == 1)

    for i, j in np.ndindex(rows, cols):
        model.AddBoolOr([x[i, j].Not(), r[i, j].Not()])

        model.Add(z[i, j] == 0).OnlyEnforceIf(x[i, j])
        model.Add(z[i, j] != 0).OnlyEnforceIf(x[i, j].Not())

        variables = []
        if i > 0:
            v = model.NewBoolVar(f'zn_{i}_{j}')
            model.Add(z[i, j] < z[i-1, j]).OnlyEnforceIf(v)
            variables.append(v)
        if i < rows - 1:
            v = model.NewBoolVar(f'zs_{i}_{j}')
            model.Add(z[i, j] < z[i+1, j]).OnlyEnforceIf(v)
            variables.append(v)
        if j > 0:
            v = model.NewBoolVar(f'zw_{i}_{j}')
            model.Add(z[i, j] < z[i, j-1]).OnlyEnforceIf(v)
            variables.append(v)
        if j < cols - 1:
            v = model.NewBoolVar(f'ze_{i}_{j}')
            model.Add(z[i, j] < z[i, j+1]).OnlyEnforceIf(v)
            variables.append(v)

        model.AddBoolOr(variables).OnlyEnforceIf([
            x[i, j].Not(), r[i, j].Not()
        ])

    # Solving
    solver = cp_model.CpSolver()
    if timeout is not None:
        solver.parameters.max_time_in_seconds = timeout
    callback = SolutionCallback(problem, x, outputter)
    status = solver.SolveWithSolutionCallback(model, callback)
    time = solver.WallTime()

    if find_all and status == cp_model.FEASIBLE:
        if cols > 1:
            model.AddBoolOr([r[0, 0], r[0, 1]])
        elif rows > 1:
            model.AddBoolOr([r[0, 0], r[1, 0]])
        model.AddImplication(x[0, 0].Not(), r[0, 0])

        for i, j in np.ndindex(rows, cols):
            adjacents = []
            if i > 0:
                adjacents.append(z[i-1, j])
            if i < rows - 1:
                adjacents.append(z[i+1, j])
            if j > 0:
                adjacents.append(z[i, j-1])
            if j < cols - 1:
                adjacents.append(z[i, j+1])

            m = model.NewIntVar(0, board.size, f'm_{i}_{j}')
            model.AddMaxEquality(m, adjacents)
            model.Add(z[i, j] == m - 1).OnlyEnforceIf([
                x[i, j].Not(), r[i, j].Not()
            ])

        while status == cp_model.FEASIBLE:
            model.AddBoolOr([
                v.Not() if solver.BooleanValue(v) else v
                for v in x.values()
            ])
            status = solver.SolveWithSolutionCallback(model, callback)
            time += solver.WallTime()
            if status == cp_model.INFEASIBLE:
                status = cp_model.OPTIMAL
            elif status == cp_model.UNKNOWN:
                status = cp_model.FEASIBLE
                break

    with redirect_stdout(sys.stderr):
        print('Status:', solver.StatusName(status))
        if find_all:
            print('Number of solutions:', callback.number_of_solutions())
        print('Wall time:', time, 's')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
            usage='%(prog)s [-a | -o FILE] <URL or file>',
            epilog=textwrap.dedent('''\
            Pzprv3 Editor (heyawake):
              http://pzv.jp/p.html?heyawake_edit

            Pzprv3 Editor (ayeheya):
              http://pzv.jp/p.html?ayeheya_edit
            '''))
    options = parser.add_mutually_exclusive_group()
    options.add_argument('-a', '--all', action='store_true',
                         help='Find all possible solutions')
    options.add_argument('-o', '--output', dest='dest',
                         help='Output pzvrv3 file')
    parser.add_argument('-t', '--timeout', type=float,
                        help='Timeout in seconds')
    parser.add_argument('data', nargs='?', help='Pzprv3 URL or file')
    args = parser.parse_args()

    if args.data is None:
        parser.print_help()
        parser.exit()

    m = re_pzprurl.fullmatch(args.data)
    if m is not None:
        problem = load_pzprurl(*m.groups())
    else:
        with open(args.data) as f:
            problem = load_pzprv3file(f)
    if args.dest is None:
        outputter = SolutionPrinter
    else:
        outputter = Pzprv3FileOutputter(args.dest)
    solve(problem, outputter, find_all=args.all, timeout=args.timeout)
