#!/usr/bin/python

import argparse
import collections
import itertools
import re
import sys
import textwrap
from contextlib import nullcontext, redirect_stdout

import numpy as np
from ortools.sat.python import cp_model


Solution = collections.namedtuple('Solution', ('vborders', 'hborders'))
re_pzprurl = re.compile(
    r'(?:https?://pzv\.jp/p\.html\?)?slither/(\d+)/(\d+)/(.+)', re.ASCII)


def load_pzprurl(cols, rows, data):
    cols = int(cols)
    rows = int(rows)

    problem = np.full((rows, cols), -1, dtype='int8').reshape(-1)
    i = 0
    for x in data:
        if x in '01234':
            problem[i] = int(x, 16)
        elif x in '56789':
            problem[i] = int(x, 16) - 5
            i += 1
        elif x in 'abcde':
            problem[i] = int(x, 16) - 10
            i += 2
        else:
            i += int(x, 36) - 16
        i += 1

    return problem.reshape((rows, cols))


def load_pzprv3file(f):
    if f.readline() != 'pzprv3.1\n':
        raise ValueError('invalid format')
    if f.readline() != 'slither\n':
        raise ValueError('invalid format')

    rows = int(f.readline())
    cols = int(f.readline())

    problem = np.full((rows, cols), -1, dtype='int8')
    for i in range(rows):
        row = f.readline().split()
        for j, x in enumerate(row):
            if x == '.':
                continue
            problem[i, j] = int(x)

    return problem


class Pzprv3FileOutputter:
    def __init__(self, dest):
        self.dest = dest

    def __call__(self, problem, solution):
        if self.dest != '-':
            cm = open(self.dest, 'w')
        else:
            cm = nullcontext(sys.stdout)
        with cm as f, redirect_stdout(f):
            print('pzprv3.1')
            print('slither')
            print(problem.shape[0])
            print(problem.shape[1])

            for row in np.where(problem >= 0, problem, '.'):
                print(' '.join(row))

            for i in range(problem.shape[0]):
                print(' '.join('0' for i in range(problem.shape[1])))

            for row in np.where(solution.vborders, '1', '-1'):
                print(' '.join(row))

            for row in np.where(solution.hborders, '1', '-1'):
                print(' '.join(row))


def SolutionPrinter(problem, solution):
    rows, cols = problem.shape
    v, h = solution

    for i in range(rows):
        for j in range(cols):
            print('+', '-' if h[i, j] else ' ', end=' ')
        print('+')
        for j in range(cols):
            print('|' if v[i, j] else ' ', end=' ')
            print(problem[i, j] if problem[i, j] >= 0 else ' ', end=' ')
        print('|' if v[i, cols] else ' ')
    for j in range(cols):
        print('+', '-' if h[rows, j] else ' ', end=' ')
    print('+')
    print()


class SolutionCallback(cp_model.CpSolverSolutionCallback):
    def __init__(self, problem, vvars, hvars, outputter):
        super().__init__()
        self.problem = problem
        self.vvars = vvars
        self.hvars = hvars
        self.outputter = outputter
        self.nsolutions = 0

    def on_solution_callback(self):
        self.nsolutions += 1

        rows, cols = self.problem.shape

        vborders = np.empty((rows, cols+1), dtype=bool)
        for i, j in np.ndindex(vborders.shape):
            vborders[i, j] = self.BooleanValue(self.vvars[i, j])
        hborders = np.empty((rows+1, cols), dtype=bool)
        for i, j in np.ndindex(hborders.shape):
            hborders[i, j] = self.BooleanValue(self.hvars[i, j])

        solution = Solution(vborders, hborders)
        self.outputter(self.problem, solution)

    def number_of_solutions(self):
        return self.nsolutions


def solve(problem, outputter, find_all=False, timeout=None):
    rows, cols = problem.shape

    model = cp_model.CpModel()

    # Declare variables
    v = {}   # vertical line
    vd = {}  # vertical line direction
    h = {}   # horizontal line
    hd = {}  # horizontal line direction
    d = {}   # node degree is 2
    r = {}   # root node
    n = {}   # node number

    for i, j in np.ndindex(rows, cols+1):
        v[i, j] = model.NewBoolVar(f'v_{i}_{j}')
        vd[i, j] = model.NewBoolVar(f'vd_{i}_{j}')
        model.AddImplication(v[i, j].Not(), vd[i, j].Not())

    for i, j in np.ndindex(rows+1, cols):
        h[i, j] = model.NewBoolVar(f'h_{i}_{j}')
        hd[i, j] = model.NewBoolVar(f'hd_{i}_{j}')
        model.AddImplication(h[i, j].Not(), hd[i, j].Not())

    for i, j in np.ndindex(rows+1, cols+1):
        d[i, j] = model.NewBoolVar(f'd_{i}_{j}')
        r[i, j] = model.NewBoolVar(f'r_{i}_{j}')
        n[i, j] = model.NewIntVar(0, (rows+1) * (cols+1), f'n_{i}_{j}')

    # Constraint: Each node has degree of 0 or 2
    for i, j in np.ndindex(rows+1, cols+1):
        neighs = []
        dneighs = []

        if i > 0:
            neighs.append(v[i-1, j])
            dneighs.append((v[i-1, j], vd[i-1, j], 0))
        if i < rows:
            neighs.append(v[i, j])
            dneighs.append((v[i, j], vd[i, j], 1))
        if j > 0:
            neighs.append(h[i, j-1])
            dneighs.append((h[i, j-1], hd[i, j-1], 0))
        if j < cols:
            neighs.append(h[i, j])
            dneighs.append((h[i, j], hd[i, j], 1))

        model.Add(sum(neighs) == 2).OnlyEnforceIf(d[i, j])
        model.Add(sum(neighs) == 0).OnlyEnforceIf(d[i, j].Not())

        for x, y in itertools.combinations(dneighs, 2):
            if x[2] == y[2]:
                model.Add(x[1] != y[1]).OnlyEnforceIf([x[0], y[0]])
            else:
                model.Add(x[1] == y[1]).OnlyEnforceIf([x[0], y[0]])

    # Constraint: number indicate how many lines around it
    for i, j in np.ndindex(rows, cols):
        if problem[i, j] < 0:
            continue
        variables = [h[i, j], h[i+1, j], v[i, j], v[i, j+1]]
        model.Add(sum(variables) == problem[i, j])

    # Constaint: Lines must form a single loop
    model.Add(sum(r.values()) == 1)

    for i, j in np.ndindex(rows+1, cols+1):
        model.AddImplication(d[i, j].Not(), r[i, j].Not())

        model.Add(n[i, j] == 0).OnlyEnforceIf(d[i, j].Not())
        model.Add(n[i, j] == 1).OnlyEnforceIf(r[i, j])
        model.Add(n[i, j] > 1).OnlyEnforceIf([d[i, j], r[i, j].Not()])

        if i > 0:
            model.Add(n[i, j] == n[i-1, j] + 1).OnlyEnforceIf(
                [r[i, j].Not(), vd[i-1, j]])
        if i < rows:
            model.Add(n[i, j] == n[i+1, j] + 1).OnlyEnforceIf(
                [r[i, j].Not(), v[i, j], vd[i, j].Not()])
        if j > 0:
            model.Add(n[i, j] == n[i, j-1] + 1).OnlyEnforceIf(
                [r[i, j].Not(), hd[i, j-1]])
        if j < cols:
            model.Add(n[i, j] == n[i, j+1] + 1).OnlyEnforceIf(
                [r[i, j].Not(), h[i, j], hd[i, j].Not()])

    # Solving
    solver = cp_model.CpSolver()
    if timeout is not None:
        solver.parameters.max_time_in_seconds = timeout
    callback = SolutionCallback(problem, v, h, outputter)
    status = solver.SolveWithSolutionCallback(model, callback)
    time = solver.WallTime()

    if find_all and status == cp_model.FEASIBLE:
        number = (problem >= 1)
        if np.any(number):
            i, j = np.unravel_index(np.argmax(number), problem.shape)
            model.AddBoolOr([
                r[i, j], r[i+1, j], r[i, j+1], r[i+1, j+1]
            ])

        while status == cp_model.FEASIBLE:
            model.AddBoolOr([
                x.Not() if solver.BooleanValue(x) else x
                for x in itertools.chain(v.values(), h.values())
            ])
            status = solver.SolveWithSolutionCallback(model, callback)
            time += solver.WallTime()
            if status == cp_model.INFEASIBLE:
                status = cp_model.OPTIMAL
            elif status == cp_model.UNKNOWN:
                status = cp_model.FEASIBLE
                break

    with redirect_stdout(sys.stderr):
        print('Status:', solver.StatusName(status))
        if find_all:
            print('Number of solutions:', callback.number_of_solutions())
        print('Wall time:', time, 's')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
            usage='%(prog)s [-a | -o FILE] <URL or file>',
            epilog=textwrap.dedent('''\
            Pzprv3 Editor:
              http://pzv.jp/p.html?slither_edit
            '''))
    options = parser.add_mutually_exclusive_group()
    options.add_argument('-a', '--all', action='store_true',
                         help='Find all possible solutions')
    options.add_argument('-o', '--output', dest='dest',
                         help='Output pzvrv3 file')
    parser.add_argument('-t', '--timeout', type=float,
                        help='Timeout in seconds')
    parser.add_argument('data', nargs='?', help='Pzprv3 URL or file')
    args = parser.parse_args()

    if args.data is None:
        parser.print_help()
        parser.exit()

    m = re_pzprurl.fullmatch(args.data)
    if m is not None:
        problem = load_pzprurl(*m.groups())
    else:
        with open(args.data) as f:
            problem = load_pzprv3file(f)
    if args.dest is None:
        outputter = SolutionPrinter
    else:
        outputter = Pzprv3FileOutputter(args.dest)
    solve(problem, outputter, find_all=args.all, timeout=args.timeout)
